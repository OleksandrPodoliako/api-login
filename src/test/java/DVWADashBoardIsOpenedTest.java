import com.codeborne.selenide.CollectionCondition;
import org.junit.Test;

import static Utils.DVWA.WedUtil.openURLWithAPILogIn;
import static com.codeborne.selenide.Selenide.$$;

public class DVWADashBoardIsOpenedTest {

    private static final String KB_DASHBOARD_PAGE_URL = "http://localhost/dvwa/index.php";

    @Test
    public final void testKBDashBoardIsOpened() {
        openURLWithAPILogIn(KB_DASHBOARD_PAGE_URL);
        $$("#main_body").shouldHave(CollectionCondition.sizeNotEqual(0));
    }

}
