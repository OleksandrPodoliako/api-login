import com.codeborne.selenide.*;
import org.junit.Test;

import static Utils.KB.WedUtil.openURLWithAPILogIn;
import static com.codeborne.selenide.Selenide.$$;

public class KBDashBoardIsOpenedTest {

    private static final String KB_DASHBOARD_PAGE_URL = "http://localhost/kanboard/?controller=DashboardController&action=show";

    @Test
    public final void testKBDashBoardIsOpened() {
        openURLWithAPILogIn(KB_DASHBOARD_PAGE_URL);
        $$("#main").shouldHave(CollectionCondition.sizeNotEqual(0));
    }

}
