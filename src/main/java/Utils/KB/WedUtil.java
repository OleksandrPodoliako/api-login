package Utils.KB;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.Cookie;

import java.util.Map;

public class WedUtil {

    public static void openURLWithAPILogIn(String url) {
        Map<String, String> cookies = APIUtil.getCookie();
        String cookieKBSID = cookies.get("KB_SID");
        String cookieKBRM = cookies.get("KB_RM");
        Selenide.open(url);
        WebDriverRunner.getWebDriver().manage().addCookie(new Cookie("KB_SID", cookieKBSID));
        WebDriverRunner.getWebDriver().manage().addCookie(new Cookie("KB_RM", cookieKBRM));
        Selenide.open(url);
    }

}
