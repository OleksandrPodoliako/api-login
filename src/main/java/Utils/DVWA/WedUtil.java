package Utils.DVWA;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.Cookie;

import java.util.Map;

public class WedUtil {

    public static void openURLWithAPILogIn(String url) {
        Map<String, String> cookies = APIUtil.getCookie();
        String cookieSecurity = cookies.get("security");
        String cookieSessionId = cookies.get("PHPSESSID");
        Selenide.open(url);
        WebDriverRunner.getWebDriver().manage().addCookie(new Cookie("security", cookieSecurity));
        WebDriverRunner.getWebDriver().manage().addCookie(new Cookie("PHPSESSID", cookieSessionId));
        Selenide.open(url);
    }

}
