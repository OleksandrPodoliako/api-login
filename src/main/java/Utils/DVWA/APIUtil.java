package Utils.DVWA;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.jsoup.Jsoup;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class APIUtil {
    private static final String BASE_URL = "http://127.0.0.1/dvwa/login.php";

    public static Map<String, String> getCookie() {
        RestAssured.requestSpecification = new RequestSpecBuilder().setBaseUri("api").setContentType(ContentType.JSON)
                .build().log().all();

        Response response01 = given()
                .when()
                .get(BASE_URL);

        String userToken = Jsoup.parseBodyFragment(response01.body().asString())
                .getElementsByAttributeValue("name", "user_token").attr("value");

        String security = response01.getCookie("security");
        String cookieSessionId = response01.getCookie("PHPSESSID");

        Response response02 = RestAssured
                .given()
                .config(RestAssured.config()
                        .encoderConfig(EncoderConfig.encoderConfig().encodeContentTypeAs("x-www-form-urlencoded", ContentType.URLENC)))
                .contentType("application/x-www-form-urlencoded; charset=UTF-8")
                .formParam("Login", "Login")
                .formParam("username", "admin")
                .formParam("password", "password")
                .formParam("user_token", userToken)
                .cookie("security", security)
                .cookie("PHPSESSID", cookieSessionId)
                .when()
                .post(BASE_URL);

        Map<String, String> cookies = new HashMap<>();
        cookies.put("security", security);
        cookies.put("PHPSESSID", cookieSessionId);
        return cookies;
    }
}
